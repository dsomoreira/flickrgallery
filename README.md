# flickrgallery


MVI based application for browsing an infinite scrolling gallery of images with basic info about each picture.

* Glide was used for the loading and caching of each picture request.

* Retrofit for building the API services from Flickr.

* Koin for dependency injection to help simplify the MVI architeture usage.

* RxJava for handling all the asynchronous calls to the Flickr API.

Link for release APK : https://we.tl/t-IDsXwWX0uV

