package com.example.flickrgallery


interface DefaultViewState {
    object Loading : DefaultViewState

    class UpdateData(val data: Any) : DefaultViewState

    class DataReceived(val data: Any) : DefaultViewState

    class Error(val error: Throwable) : DefaultViewState
}