package com.example.flickrgallery

import com.example.flickrgallery.api.retrofit.FlickrService
import com.example.flickrgallery.api.retrofit.RetrofitBuilder
import com.example.flickrgallery.api.models.photos.PhotosResponse
import com.example.flickrgallery.api.models.user.UserResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent

class FlickrAPI: KoinComponent {

    private val api: FlickrService by lazy { RetrofitBuilder.createFlickrService() }

    fun getUserId(username: String): Observable<UserResponse> = api.findUsername(username).subscribeOn(Schedulers.io()).observeOn(
        AndroidSchedulers.mainThread())

    fun getPublicPhotos(id: String, safeSearch: String? = null, perPage: String, pageNum: String): Observable<PhotosResponse> = api.getPublicPhotos(id, safeSearch, perPage, pageNum).
        subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun getPhotoInfo(id: String, secret: String?) = api.getPhotoInfo(id, secret).
        subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun getPhotoSize(id: String) = api.getPhotoSize(id).
        subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}