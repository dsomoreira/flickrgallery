package com.example.flickrgallery

import com.example.flickrgallery.menus.home.MainInteractor
import com.example.flickrgallery.menus.home.MainPresenter
import com.example.flickrgallery.menus.home.PhotoInfoPresenter
import com.example.flickrgallery.menus.info.PhotoInfoInteractor
import com.example.flickrgallery.repository.IMainRepository
import com.example.flickrgallery.repository.MainRepository
import org.koin.dsl.module

class KoinModules {

    companion object {
        val mviPresenters = module {

            factory { MainRepository(get(),get()) as IMainRepository }
            factory { MainPresenter(MainInteractor(get())) }
            factory { PhotoInfoPresenter(PhotoInfoInteractor(get())) }

        }

        val utils = module {
            single { FlickrAPI() }
        }
    }
}