import com.google.gson.annotations.SerializedName

/*
Copyright (c) 2019 Kotlin Data Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */


data class Photo (

	@SerializedName("id") val id : String,
	@SerializedName("secret") val secret : String,
	@SerializedName("server") val server : Int,
	@SerializedName("farm") val farm : Int,
	@SerializedName("dateuploaded") val dateuploaded : Int,
	@SerializedName("isfavorite") val isfavorite : Int,
	@SerializedName("license") val license : Int,
	@SerializedName("safety_level") val safety_level : Int,
	@SerializedName("rotation") val rotation : Int,
	@SerializedName("owner") val owner : Owner,
	@SerializedName("title") val title : Title,
	@SerializedName("description") val description : Description,
	@SerializedName("visibility") val visibility : Visibility,
	@SerializedName("dates") val dates : Dates,
	@SerializedName("views") val views : Int,
	@SerializedName("editability") val editability : Editability,
	@SerializedName("publiceditability") val publiceditability : Publiceditability,
	@SerializedName("usage") val usage : Usage,
	@SerializedName("comments") val comments : Comments,
	@SerializedName("notes") val notes : Notes,
	@SerializedName("people") val people : People,
	@SerializedName("tags") val tags : Tags,
	@SerializedName("location") val location : Location,
	@SerializedName("geoperms") val geoperms : Geoperms,
	@SerializedName("urls") val urls : Urls,
	@SerializedName("media") val media : String
)