package com.example.flickrgallery.api.models.photos

import com.google.gson.annotations.SerializedName

data class Photos(
    @SerializedName("page")
    val currentPage: String,
    @SerializedName("pages")
    val pageCount: String,
    @SerializedName("perpage")
    val perPage: String,
    @SerializedName("total")
    val totalPages: String,
    @SerializedName("photo")
    val publicPhoto: Array<PublicPhoto>
)