package com.example.flickrgallery.api.models.photos

import com.google.gson.annotations.SerializedName

data class PublicPhoto(
    @SerializedName("id")
    val id: String,
    @SerializedName("owner")
    val owner: String,
    @SerializedName("secret")
    val secret: String,
    @SerializedName("server")
    val server: String,
    @SerializedName("farm")
    val farm: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("ispublic")
    val ispublic: String,
    @SerializedName("isfriend")
    val isfriend: String,
    @SerializedName("isfamily")
    val isfamily: String
)