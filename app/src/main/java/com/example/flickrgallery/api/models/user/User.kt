package com.example.flickrgallery.api.models.user

import com.google.gson.annotations.SerializedName

data class User (
    @SerializedName("id")
    val id: String,
    @SerializedName("nsid")
    val nsId: String,
    @SerializedName("username")
    val username: Username
)
