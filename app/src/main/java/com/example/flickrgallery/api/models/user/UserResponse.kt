package com.example.flickrgallery.api.models.user

import com.google.gson.annotations.SerializedName

data class UserResponse (
    @SerializedName("user")
    val username: User,
    @SerializedName("stat")
    val stat: String
)