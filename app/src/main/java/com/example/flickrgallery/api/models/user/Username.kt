package com.example.flickrgallery.api.models.user

import com.google.gson.annotations.SerializedName

data class Username(
    @SerializedName("_content")
    val content: String
)
