package com.example.flickrgallery.api.retrofit

import PhotoInfoResponse
import PhotoSizeResponse
import com.example.flickrgallery.api.models.photos.PhotosResponse
import com.example.flickrgallery.api.models.user.UserResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

// https://api.flickr.com/services
// REST ENDPOINT = https://www.flickr.com/services/rest/
//
interface FlickrService {

    @GET("/services/rest/?method=flickr.people.findByUsername")
    fun findUsername(@Query("username") username: String): Observable<UserResponse>

    @GET("/services/rest/?method=flickr.people.getPublicPhotos")
    fun getPublicPhotos(
        @Query("user_id") id: String,
        @Query("safe_search") safeSearch: String?,
        @Query("per_page") perPage: String,
        @Query("page") pageNum: String
    ): Observable<PhotosResponse>

    @GET("/services/rest/?method=flickr.photos.getInfo")
    fun getPhotoInfo(@Query("photo_id") id: String, @Query("secret") secret: String?): Observable<PhotoInfoResponse>

    @GET("/services/rest/?method=flickr.photos.getSizes")
    fun getPhotoSize(@Query("photo_id") id: String): Observable<PhotoSizeResponse>

}

// 1 = SAFE, 2 = MODERATE, 3 = RESTRICTED
enum class SafeSearchEnum {
    SAFE, MODERATE, RESTRICTED
}
