package com.example.flickrgallery.api.retrofit

import com.example.flickrgallery.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val apiKey = "4c4738e740b4f47f21be3481babb7d3e"
    private const val secret = "a7a34909c979d6fe"

    fun createFlickrService(): FlickrService {

        val client = OkHttpClient().newBuilder()
            .addInterceptor(FlickrRequestInterceptor(apiKey))
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            .build()


        return Retrofit.Builder()
            .baseUrl("https://www.flickr.com")
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(FlickrService::class.java)
    }

}
