package com.example.flickrgallery.menus.home

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.FragmentTransaction
import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.KoinModules
import com.example.flickrgallery.R
import com.example.flickrgallery.menus.info.PhotoInfoFragment
import com.example.flickrgallery.menus.models.FetchPageModel
import com.example.flickrgallery.menus.models.PhotoModel
import com.example.flickrgallery.mvi.MainMviView
import com.hannesdorfmann.mosby3.mvi.MviActivity
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainActivity : MviActivity<MainMviView, MainPresenter>(), MainMviView {

    private val fetchNextPage = BehaviorSubject.create<FetchPageModel>()
    private val loadIntent = BehaviorSubject.create<FetchPageModel>()

    private lateinit var adapter: PhotoAdapter

    private val DEFAULT_USERNAME = "eyetwist"
    private val perPage = 100
    private var page = 1

    override fun createPresenter(): MainPresenter = getKoin().get()

    override fun loadIntent(): Observable<FetchPageModel> = loadIntent

    override fun fetchNextPage(): Observable<FetchPageModel> = fetchNextPage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadIntent.onNext(FetchPageModel(DEFAULT_USERNAME, perPage, page))

        hideContentsOnThumbnail()

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        startKoin {
            modules(listOf(KoinModules.mviPresenters, KoinModules.utils))
                .androidContext(applicationContext)
        }

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            toggleToolbar(true)
        } else
            super.onBackPressed()
    }

    override fun render(viewState: DefaultViewState) {
        when (viewState) {

            is DefaultViewState.Loading -> {
                toggleLoading(true)
            }

            is DefaultViewState.DataReceived -> {

                val photos = (viewState.data as MutableList<PhotoModel>)
                adapter = PhotoAdapter(photos, applicationContext)

                adapter.setOnPhotoClicked {
                    toggleToolbar(false)

                    val fragment = PhotoInfoFragment.newInstance(it)

                    fragment.setOnBackListener {
                        supportFragmentManager.popBackStack()
                        toggleToolbar(true)
                    }

                    supportFragmentManager.beginTransaction()
                        .add(fragmentContainer.id, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(null)
                        .commit()
                }

                adapter.setOnBottomReachedListener {
                    fetchNextPage.onNext(FetchPageModel(DEFAULT_USERNAME, perPage, ++page))
                }

                photoRv.adapter = adapter

                toggleLoading(false)
            }

            is DefaultViewState.UpdateData -> {
                val photos = (viewState.data as List<PhotoModel>)
                adapter.addPhotos(photos)
                toggleLoading(false)
            }

            is DefaultViewState.Error -> {
                viewState.error.printStackTrace()
                toggleLoading(false)
            }
        }
    }

    private fun toggleLoading(boolean: Boolean) {
        if (boolean)
            loadingSpinner.visibility = View.VISIBLE
        else
            loadingSpinner.visibility = View.GONE
    }

    private fun toggleToolbar(boolean: Boolean) {
        if (boolean)
            toolbar.visibility = View.VISIBLE
        else
            toolbar.visibility = View.GONE
    }

    private fun hideContentsOnThumbnail() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )
    }

}
