package com.example.flickrgallery.menus.home

import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.api.models.photos.PublicPhoto
import com.example.flickrgallery.menus.models.PhotoModel
import com.example.flickrgallery.repository.IMainRepository
import io.reactivex.Observable

class MainInteractor(private val repository: IMainRepository) {

    fun getUserPhotos(username: String, perPage: String, pageNum: String) : Observable<DefaultViewState> {
        return repository.getUserId(username)
            .flatMap {
                repository.getUserPhotos(it.username.id, perPage, pageNum).flatMap { response ->
                    if(pageNum.toInt() == 1)
                        Observable.just(DefaultViewState.DataReceived(buildPhotoUrls(response.photos.publicPhoto)) as DefaultViewState)
                    else
                        Observable.just(DefaultViewState.UpdateData(buildPhotoUrls(response.photos.publicPhoto)) as DefaultViewState)
                }

            }
            .startWith(DefaultViewState.Loading as DefaultViewState)
            .onErrorReturn {
                DefaultViewState.Error(it)
            }
    }

    // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
    private fun buildPhotoUrls(list: Array<PublicPhoto>): MutableList<PhotoModel> {

        val urlList: MutableList<PhotoModel> =  mutableListOf()

        // Getting smaller size photos for thumbnails
        list.forEach {
            val string = StringBuilder().apply {
                append("https://farm")
                append(it.farm)
                append(".staticflickr.com/")
                append(it.server)
                append("/")
                append(it.id)
                append("_")
                append(it.secret)
                append("_")
                append("n")
                append(".jpg")
            }
            urlList.add(
                PhotoModel(
                    it.id,
                    string.toString()
                )
            )
        }

        return urlList
    }
}