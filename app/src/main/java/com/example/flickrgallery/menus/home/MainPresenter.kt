package com.example.flickrgallery.menus.home

import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.mvi.MainMviView
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.*

class MainPresenter(private val interactor: MainInteractor) : MviBasePresenter<MainMviView, DefaultViewState>() {
    override fun bindIntents() {

        val loadIntent = intent(MainMviView::loadIntent).switchMap { interactor.getUserPhotos(it.username, it.perPage.toString(), it.page.toString()) }

        val fetchNextPageIntent = intent(MainMviView::fetchNextPage).switchMap { interactor.getUserPhotos(it.username, it.perPage.toString(), it.page.toString()) }

        val viewState = Observable.merge(fetchNextPageIntent,loadIntent).observeOn(AndroidSchedulers.mainThread()).distinctUntilChanged()

        subscribeViewState(viewState, MainMviView::render)
    }
}