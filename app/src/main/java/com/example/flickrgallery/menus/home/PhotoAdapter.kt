package com.example.flickrgallery.menus.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.flickrgallery.menus.models.PhotoModel
import kotlinx.android.synthetic.main.photo_item.view.*


class PhotoAdapter (private var items: MutableList<PhotoModel>, private val context: Context) :
    RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {

    private lateinit var onPhotoClickedListener: (PhotoModel) -> Unit
    private lateinit var onBottomReachedListener: (Unit) -> Unit


    override fun getItemCount() = items.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PhotoViewHolder(LayoutInflater.from(context).inflate(com.example.flickrgallery.R.layout.photo_item, parent, false))


    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {

        val item = items[position]
        val view = holder.itemView

        Glide.with(context)
            .load(item.url)
            .placeholder(com.example.flickrgallery.R.drawable.ic_launcher_background)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view.image)

        view.image.setOnClickListener {

            onPhotoClickedListener.invoke(
                PhotoModel(
                    item.id,
                    item.url
                )
            )
        }

        if(position == items.size - 6)
            onBottomReachedListener.invoke(Unit)

    }

    fun setOnPhotoClicked(listener: (PhotoModel) -> Unit) {
        onPhotoClickedListener = listener
    }

    fun setOnBottomReachedListener(listener: (Unit) -> Unit) {
        onBottomReachedListener = listener
    }

    fun addPhotos(photos: List<PhotoModel>) {
        val oldSize = items.size
        items.addAll(photos)
        notifyItemRangeInserted(oldSize, oldSize + photos.size)
    }

    inner class PhotoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
