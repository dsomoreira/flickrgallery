package com.example.flickrgallery.menus.info

import Location
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.target.DrawableImageViewTarget
import com.bumptech.glide.request.transition.Transition
import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.R
import com.example.flickrgallery.menus.home.PhotoInfoPresenter
import com.example.flickrgallery.menus.models.InfoModel
import com.example.flickrgallery.menus.models.PhotoModel
import com.example.flickrgallery.mvi.PhotoInfoMviView
import com.hannesdorfmann.mosby3.mvi.MviFragment
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_photo_info.*
import org.koin.android.ext.android.getKoin


class PhotoInfoFragment : MviFragment<PhotoInfoMviView, PhotoInfoPresenter>(), PhotoInfoMviView {

    private val photoModel by lazy { arguments?.get("PhotoModel") as PhotoModel }

    private lateinit var onBackListener: () -> Unit

    override fun loadIntent() = Observable.just(photoModel.id)

    override fun createPresenter(): PhotoInfoPresenter = getKoin().get()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_photo_info, container, false).apply {

            setOnClickListener {
                onBackListener.invoke()
            }
        }
    }

    override fun render(viewState: DefaultViewState) {
        when(viewState){

            is DefaultViewState.Loading -> {
            }

            is DefaultViewState.DataReceived -> {
                val data = viewState.data as InfoModel
                loadPhotoFromUrl(data)
            }

            is DefaultViewState.Error -> {
                failedLoadText.visibility = View.VISIBLE
                viewState.error.printStackTrace()
            }
        }
    }

    private fun loadPhotoFromUrl(data: InfoModel) {

        Glide.with(activity!!.applicationContext)
            .load(data.url)
            .fitCenter()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(object : DrawableImageViewTarget(fullImage) {

                override fun onLoadStarted(placeholder: Drawable?) {
                    super.onLoadStarted(placeholder)
                    loadingSpinnerInfo.visibility = View.VISIBLE
                }

                override fun onLoadFailed(placeholder: Drawable?) {
                    super.onLoadStarted(placeholder)
                    loadingSpinnerInfo.visibility = View.GONE
                    failedLoadText.visibility = View.VISIBLE
                }

                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable>?
                ) {
                    super.onResourceReady(resource, transition)
                    fullImage.setImageDrawable(resource)
                    loadingSpinnerInfo.visibility = View.GONE
                    setPhotoInfo(data)
                }
            })
    }

    private fun setPhotoInfo(info: InfoModel){
        info.apply {
            titleText.text = title ?: "No title"
            descText.text = description ?: ""
            locationText.text = getLocation(info.location)
            viewsText.text = views ?: "N/A"
            postedText.text = posted ?: "N/A"

            locationIcon.visibility = View.VISIBLE
            viewsIcon.visibility = View.VISIBLE
            postedIcon.visibility = View.VISIBLE
        }
    }

    //"${county._content} - ${region._content}, ${country._content}"
    private fun getLocation(locality: Location?) : String{
        val location = StringBuilder()

        locality?.apply {
            county?.let { location.append(it._content) }
            region?.let { location.append(" - "); location.append(it._content) }
            country?.let { location.append(", "); location.append(it._content) }
        }

        if(location.toString().isNotEmpty())
            return location.toString()
        else
            return "No location"
}

    fun setOnBackListener(listener: () -> Unit) {
        onBackListener = listener
    }

    companion object{
        fun newInstance(photo: PhotoModel): PhotoInfoFragment {
            val fragment = PhotoInfoFragment()
            val args = Bundle()
            args.putSerializable("PhotoModel", photo)
            fragment.arguments = args
            return fragment
        }
    }

}