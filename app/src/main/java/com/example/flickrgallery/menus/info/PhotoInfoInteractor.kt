package com.example.flickrgallery.menus.info

import PhotoInfoResponse
import PhotoSizeResponse
import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.menus.models.InfoModel
import com.example.flickrgallery.repository.IMainRepository
import io.reactivex.Observable
import io.reactivex.functions.BiFunction

class PhotoInfoInteractor(private val repository: IMainRepository) {

    fun getPhotoInfo(id: String, secret: String?) : Observable<DefaultViewState> =
        Observable.zip(
            repository.getPhotoInfo(id, secret),
            repository.getPhotoSize(id),
            BiFunction { info: PhotoInfoResponse, size: PhotoSizeResponse ->
                val photoInfo: InfoModel

                // Get largest image available
                val url = size.sizes.size.last().source

                info.photo.apply {
                    photoInfo = InfoModel(url,title._content, description._content, dates.taken, views.toString(), location)
                }

                DefaultViewState.DataReceived(photoInfo) as DefaultViewState
            })
            .onErrorReturn {
                DefaultViewState.Error(it) as DefaultViewState

            }.startWith(DefaultViewState.Loading as DefaultViewState)

}