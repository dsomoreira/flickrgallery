package com.example.flickrgallery.menus.home

import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.menus.info.PhotoInfoInteractor
import com.example.flickrgallery.mvi.PhotoInfoMviView
import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers

class PhotoInfoPresenter(private val interactor: PhotoInfoInteractor) : MviBasePresenter<PhotoInfoMviView, DefaultViewState>() {
    override fun bindIntents() {

        val getPhotoInfoIntent = intent(PhotoInfoMviView::loadIntent).switchMap { interactor.getPhotoInfo(it, null) }

        val viewState = getPhotoInfoIntent.observeOn(AndroidSchedulers.mainThread())

        subscribeViewState(viewState, PhotoInfoMviView::render)
    }
}