package com.example.flickrgallery.menus.models

data class FetchPageModel(val username: String, val perPage: Int, val page: Int)