package com.example.flickrgallery.menus.models

import Location

data class InfoModel(
    val url: String,
    val title: String?,
    val description: String?,
    val posted: String?,
    val views: String?,
    val location: Location?
)