package com.example.flickrgallery.menus.models

import java.io.Serializable

data class PhotoModel(val id: String, val url: String) : Serializable