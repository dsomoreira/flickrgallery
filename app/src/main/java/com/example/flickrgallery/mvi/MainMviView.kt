package com.example.flickrgallery.mvi

import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.menus.models.FetchPageModel
import com.example.flickrgallery.mvi.MviView
import io.reactivex.Observable

interface MainMviView : MviView<DefaultViewState> {
    fun loadIntent(): Observable<FetchPageModel>
    fun fetchNextPage(): Observable<FetchPageModel>
}