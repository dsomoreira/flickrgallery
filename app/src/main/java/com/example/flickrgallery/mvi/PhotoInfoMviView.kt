package com.example.flickrgallery.mvi

import com.example.flickrgallery.DefaultViewState
import com.example.flickrgallery.mvi.MviView
import io.reactivex.Observable

interface PhotoInfoMviView : MviView<DefaultViewState> {
    fun loadIntent(): Observable<String>
}