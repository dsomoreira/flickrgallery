package com.example.flickrgallery.repository

import PhotoInfoResponse
import PhotoSizeResponse
import android.graphics.Bitmap
import com.bumptech.glide.request.FutureTarget
import com.example.flickrgallery.api.models.photos.PhotosResponse
import com.example.flickrgallery.api.models.user.UserResponse
import io.reactivex.Observable

interface IMainRepository {

    fun getUserId(username: String): Observable<UserResponse>
    fun getUserPhotos(id: String, perPage: String, pageNum: String): Observable<PhotosResponse>
    fun getPhotoInfo(id: String, secret: String?): Observable<PhotoInfoResponse>
    fun getPhotoSize(id: String): Observable<PhotoSizeResponse>
    fun getPhoto(url: String): Observable<FutureTarget<Bitmap>>
}