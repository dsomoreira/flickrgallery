package com.example.flickrgallery.repository

import PhotoInfoResponse
import PhotoSizeResponse
import android.content.Context
import android.graphics.Bitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.request.FutureTarget
import com.example.flickrgallery.FlickrAPI
import io.reactivex.Observable

class MainRepository(private val context: Context, private val api: FlickrAPI) : IMainRepository {

    override fun getUserId(username: String) = api.getUserId(username)


    override fun getUserPhotos(id: String, perPage: String, pageNum: String) = api.getPublicPhotos(id, "0", perPage, pageNum)


    override fun getPhotoInfo(id: String, secret: String?): Observable<PhotoInfoResponse> = api.getPhotoInfo(id, secret)


    override fun getPhotoSize(id: String): Observable<PhotoSizeResponse> = api.getPhotoSize(id)

    override fun getPhoto(url: String): Observable<FutureTarget<Bitmap>> {
        return Observable.just(Glide.with(context)
            .asBitmap().load(url).submit())
    }

}